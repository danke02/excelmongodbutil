﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using System.Threading;
using MongoDB.Driver;
using MongoDB.Bson;
using ExcelMongoDBUtilNS;

namespace MongoDBRTDServerLibrary
{
    //http://stackoverflow.com/questions/5397607/how-do-i-create-a-real-time-excel-automation-add-in-in-c-sharp-using-rtdserver

    [Guid("EAC86BF6-06BC-469D-BF51-04A2A5CDA1A2")]
    [ProgId("MongoDBRTDServer.ProgId")]
    public class MongoDBRTDServer : IRtdServer
    {
        private readonly Dictionary<int, MongoDBQueryInfo> _topics = new Dictionary<int, MongoDBQueryInfo>();
        private Timer _timer;

        public dynamic ConnectData(int TopicID, ref Array Strings, ref bool GetNewValues)
        {
            GetNewValues = true;
            MongoDBQueryInfo c = new MongoDBQueryInfo();
            c.server = Strings.GetValue(0).ToString();
            c.database = Strings.GetValue(1).ToString();
            c.collection = Strings.GetValue(2).ToString();
            c.field = Strings.GetValue(3).ToString();
            int condCount = (Strings.Length - 4) / 2;
            c.condFields = new string[condCount];
            c.condValues = new string[condCount];
            for (int i = 0; i < condCount; ++i)
            {
                c.condFields[i] = Strings.GetValue(4 + i * 2).ToString();
                c.condValues[i] = Strings.GetValue(4 + i * 2 + 1).ToString();
            }
            int result = MongoDBWrapper.GetInstance().Select(c);
            
            _topics[TopicID] = c;

            return _topics[TopicID].value;
        }

        public void DisconnectData(int TopicID)
        {
            _topics.Remove(TopicID);
        }

        public int Heartbeat()
        {
            return 1;
        }

        public Array RefreshData(ref int topicCount)
        {
            var data = new object[2, _topics.Count];
            var index = 0;

            foreach (var entry in _topics)
            {
                MongoDBWrapper.GetInstance().Select(entry.Value);
                data[0, index] = entry.Key;
                data[1, index] = entry.Value.value;
                ++index;
            }

            topicCount = _topics.Count;

            return data;
        }

        public int ServerStart(IRTDUpdateEvent rtdUpdateEvent)
        {
            _timer = new Timer(delegate
            {
                try
                {
                    rtdUpdateEvent.UpdateNotify();
                }
                catch (COMException ce)
                {
                    System.Diagnostics.Debug.WriteLine(ce.ToString());
                }
            }, null, TimeSpan.Zero, TimeSpan.FromSeconds(5));
            return 1;
        }

        public void ServerTerminate()
        {
            _timer.Dispose();
        }
        [ComRegisterFunctionAttribute]
        public static void RegisterFunction(Type t)
        {
            Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(@"CLSID\{" + t.GUID.ToString().ToUpper() + @"}\Programmable");
            var key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"CLSID\{" + t.GUID.ToString().ToUpper() + @"}\InprocServer32", true);
            if (key != null)
                key.SetValue("", System.Environment.SystemDirectory + @"\mscoree.dll", Microsoft.Win32.RegistryValueKind.String);
        }

        [ComUnregisterFunctionAttribute]
        public static void UnregisterFunction(Type t)
        {
            Microsoft.Win32.Registry.ClassesRoot.DeleteSubKey(@"CLSID\{" + t.GUID.ToString().ToUpper() + @"}\Programmable");
        }
    }
}