﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelMongoDBUtilNS;
namespace ExcelMongoDBUtilTester
{
    class Program
    {
        static void Main(string[] args)
        {
            ExcelMongoDBUtil dbUtil = new ExcelMongoDBUtil();
            string server = "mongodb://localhost:27017";
            dbUtil.UpdateField(server, "batch", "option_vol", new string[] { "matDate","underlyingIsinCode","strikePrice" }, new object[] { "201701","KR7005930003", 1350000.0}, new string[] { "vol", "volType" }, new object[] { 0.2,"P" });


            //for (int i = 0; i < 10; ++i)
            //{
            //    string name = dbUtil.ReadField(server, "local", "test", new string[] { "code" }, new object[] { "1234" }, "name").ToString();
            //    Console.WriteLine(i + " ret:" + name);
            //    dbUtil.UpdateField(server, "local", "test", new string[] { "code" }, new object[] { "1234" }, new string[] { "name","age" }, new object[] { name + i ,i});
            //}
            //object ret = (object)(new object[1000]);
            //int count = dbUtil.ReadFieldMany(server, "local", "futuresInfo", new string[] { "code"}, new string[] { "/4205/"}, "isinCode", ref ret);
            Console.ReadLine();
        }
    }
}