﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelMongoDBUtilNS
{
    public class MongoDBQueryInfo
    {
        public string server;
        public string database;
        public string collection;
        public string[] condFields;
        public object[] condValues;

        public string[] updateFields;
        public object[] updateValues;

        public string rawJson;

        public string field;
        public string[] fields;
        public object value;
    }
}
