﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelMongoDBUtilNS
{
    public class MongoDBWrapper
    {
        ConcurrentDictionary<string, MongoClient> clientDic;
        static MongoDBWrapper instance = null;
        static object syncObj = new object();
        public static MongoDBWrapper GetInstance()
        {
            lock (syncObj)
            {
                if (instance == null)
                    instance = new MongoDBWrapper();
                return instance;
            }
        }
        private MongoDBWrapper()
        {
            clientDic = new ConcurrentDictionary<string, MongoClient>();
        }
        public long Insert(MongoDBQueryInfo c)
        {
            try
            {
                if (Select(c) > 1)
                    return -1;
                MongoClient cli = clientDic.GetOrAdd(c.server, (serverAddr) => new MongoClient(serverAddr));

                var testdb = cli.GetDatabase(c.database);
                var collection = testdb.GetCollection<BsonDocument>(c.collection);
                BsonDocument doc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(c.rawJson);
                collection.InsertOne(doc);
                return 1;
            }
            catch (Exception e)
            {

            }
            return -1;
        }
        public long Update(MongoDBQueryInfo c)
        {
            try
            {
                if (Select(c) > 1)
                    return -1;
                MongoClient cli = clientDic.GetOrAdd(c.server, (serverAddr) => new MongoClient(serverAddr));

                var testdb = cli.GetDatabase(c.database);
                var collection = testdb.GetCollection<BsonDocument>(c.collection);
                var builder = Builders<BsonDocument>.Filter;
                FilterDefinition<BsonDocument> filter = null;
                for (int i = 0; i < c.condFields.Length; ++i)
                {
                    if (i == 0)
                    {
                        if (c.condValues[i] is string)
                            filter = builder.Regex(c.condFields[i], BsonRegularExpression.Create(c.condValues[i]));
                        else
                            filter = builder.Eq(c.condFields[i], c.condValues[i]);
                    }
                    else
                    {
                        if (c.condValues[i] is string)
                            filter = filter & builder.Regex(c.condFields[i], BsonRegularExpression.Create(c.condValues[i]));
                        else
                            filter = filter & builder.Eq(c.condFields[i], c.condValues[i]);
                    }
                }
                if (filter == null)
                    filter = builder.Empty;


                UpdateDefinition<BsonDocument> update = null;
                for (int i = 0; i < c.updateFields.Length; ++i)
                {
                    if (i == 0)
                        update = Builders<BsonDocument>.Update.Set(c.updateFields[i], c.updateValues[i]);
                    else
                        update = update.Set(c.updateFields[i], c.updateValues[i]);
                }
                var result = collection.UpdateOne(filter, update, new UpdateOptions { IsUpsert = true });

                return result.MatchedCount;

            }
            catch (Exception e)
            {

            }
            return -1;
        }
        public int Select(MongoDBQueryInfo c)
        {
            try
            {
                MongoClient cli = clientDic.GetOrAdd(c.server, (serverAddr) => new MongoClient(serverAddr));

                var testdb = cli.GetDatabase(c.database);
                var collection = testdb.GetCollection<BsonDocument>(c.collection);
                var builder = Builders<BsonDocument>.Filter;
                FilterDefinition<BsonDocument> filter = null;
                for (int i = 0; i < c.condFields.Length; ++i)
                {
                    if (i == 0)
                    {
                        if (c.condValues[i] is string)
                            filter = builder.Regex(c.condFields[i], BsonRegularExpression.Create(c.condValues[i]));
                        else
                            filter = builder.Eq(c.condFields[i], c.condValues[i]);
                    }
                    else
                    {
                        if (c.condValues[i] is string)
                            filter = filter & builder.Regex(c.condFields[i], BsonRegularExpression.Create(c.condValues[i]));
                        else
                            filter = filter & builder.Eq(c.condFields[i], c.condValues[i]);
                    }
                }
                if (filter == null)
                    filter = builder.Empty;

                var result = collection.Find(filter).ToList();
                if (result.Count == 1)
                {
                    c.value = result[0][c.field];
                    return result.Count;
                }
            }
            catch (Exception e)
            {

            }
            c.value = ExcelMongoDBUtil.ToExcelError(ExcelMongoDBUtil.ExcelErrorCodes.xlErrNA);

            return -1;
        }
        
        public object[,] SelectMany(MongoDBQueryInfo c)
        {
            object[,] ret = null;
            try
            {

                MongoClient cli = clientDic.GetOrAdd(c.server, (serverAddr) => new MongoClient(serverAddr));

                var testdb = cli.GetDatabase(c.database);
                var collection = testdb.GetCollection<BsonDocument>(c.collection);
                var builder = Builders<BsonDocument>.Filter;
                FilterDefinition<BsonDocument> filter = null;
                for (int i = 0; i < c.condFields.Length; ++i)
                {
                    if (c.condFields[i] == "")
                    {
                        filter = null;
                        break;
                    }
                    if (i == 0)
                    {
                        if (c.condValues[i] is string)
                            filter = builder.Regex(c.condFields[i], BsonRegularExpression.Create(c.condValues[i]));
                        else
                            filter = builder.Eq(c.condFields[i], c.condValues[i]);
                    }
                    else
                    {
                        if (c.condValues[i] is string)
                            filter = filter & builder.Regex(c.condFields[i], BsonRegularExpression.Create(c.condValues[i]));
                        else
                            filter = filter & builder.Eq(c.condFields[i], c.condValues[i]);
                    }
                }
                if (filter == null)
                    filter = builder.Empty;

                var result = collection.Find(filter).ToList();
                ret = new object[result.Count, c.fields.Length];
                for (int i = 0; i < result.Count; ++i)
                {
                    for (int j = 0; j < c.fields.Length; ++j)
                    {
                        if (result[i].Contains(c.fields[j]))
                            ret[i, j] = result[i][c.fields[j]];
                        else
                            ret[i, j] = null;
                    }
                }
            }
            catch (Exception e)
            {

            }
            return ret;
        }
    }
}