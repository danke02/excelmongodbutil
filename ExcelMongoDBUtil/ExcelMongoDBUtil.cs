﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
namespace ExcelMongoDBUtilNS
{
    [ComVisible(true)]
    [Guid("A7737A08-F2FF-4E00-879C-D6BEEB4DFFA1")]
    [ClassInterface(ClassInterfaceType.None)]
    public class ExcelMongoDBUtil
    {
        public  enum ExcelErrorCodes
        {
            xlErrDiv0 = 0x7d7,
            xlErrNA = 0x7fa,
            xlErrName = 0x7ed,
            xlErrNull = 0x7d0,
            xlErrNum = 0x7f4,
            xlErrRef = 0x7e7,
            xlErrValue = 0x7df
        }

        /// <summary> 
        /// Retruns the specified cell error value in the Excel-recognizable form. 
        /// </summary> 
        /// <param name="xlError">The cell error value to return.</param> 
        public static object ToExcelError(ExcelErrorCodes xlError)
        {
            int hr = 0x800A;
            return new System.Runtime.InteropServices.ErrorWrapper((int)((hr << 16) | (int)xlError));
        }
        //http://www.codeproject.com/Articles/12386/Sending-an-array-of-doubles-from-Excel-VBA-to-C-us
        private static object[] GetObjectArrayFromComObject(object comObject)
        {
            Type t = Type.GetType("System.Object[*]");
            int length = (int)t.InvokeMember("Length", System.Reflection.BindingFlags.GetProperty, null, comObject, null);
            object[] ret = new object[length];
            for (int i = 0; i < length; ++i)
            {
                ret[i] = t.InvokeMember("GetValue", System.Reflection.BindingFlags.InvokeMethod, null, comObject, new object[] { i });
            }
            return ret;
        }
        private static string[] GetStringArrayFromComObject(object comObject)
        {
            Type t = Type.GetType("System.String[*]");
            int length = (int)t.InvokeMember("Length", System.Reflection.BindingFlags.GetProperty, null, comObject, null);
            string[] ret = new string[length];
            for (int i = 0; i < length; ++i)
            {
                ret[i] = t.InvokeMember("GetValue", System.Reflection.BindingFlags.InvokeMethod, null, comObject, new object[] { i }).ToString();
            }
            return ret;
        }
        private static int UpdateComObjectWithArray(object comObject, object[,] objList)
        {
            Type t = Type.GetType("System.Object[*]");
            int length = (int)t.InvokeMember("Length", System.Reflection.BindingFlags.GetProperty, null, comObject, null);
            for (int i = 0; i < length && i <= objList.GetUpperBound(0); ++i)
            {
                for (int j = 0; j <= objList.GetUpperBound(1); ++j)
                    ((object[,])comObject)[i, j] = objList[i, j];
            }
            return objList.GetUpperBound(0) + 1;
        }
        [ComVisible(true)]
        public object ReadField(string server, string database, string collection, object condFields, object condValues, string field)
        {try
            {
                MongoDBQueryInfo c = new MongoDBQueryInfo();
                c.server = server;
                c.collection = collection;
                c.database = database;
                c.field = field;
                c.condFields = GetStringArrayFromComObject(condFields);
                c.condValues = GetObjectArrayFromComObject(condValues);
                c.field = field;
                if (MongoDBWrapper.GetInstance().Select(c) != 1)
                    return ToExcelError(ExcelErrorCodes.xlErrNA);
                return c.value;
            }
            catch(Exception e)
            {
                return ToExcelError(ExcelErrorCodes.xlErrNA);
            }
        }
        [ComVisible(true)]
        public int ReadFieldMany(string server, string database, string collection, object condFields, object condValues, object fields,  ref object ret)
        {
            try
            {
                MongoDBQueryInfo c = new MongoDBQueryInfo();
                c.server = server;
                c.collection = collection;
                c.database = database;
                
                c.condFields = GetStringArrayFromComObject(condFields);
                c.condValues = GetObjectArrayFromComObject(condValues);

                //object[] test = GetStringArrayFromComObject(ret);
                c.fields = GetStringArrayFromComObject(fields);
                return UpdateComObjectWithArray(ret, MongoDBWrapper.GetInstance().SelectMany(c));
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        [ComVisible(true)]
        public long UpdateField(string server, string database, string collection, object condFields, object condValues, object updateFields, object updateValues)
        {
            try
            {
                MongoDBQueryInfo c = new MongoDBQueryInfo();
                c.server = server;
                c.collection = collection;
                c.database = database;
                c.condFields = GetStringArrayFromComObject(condFields);
                c.condValues = GetObjectArrayFromComObject(condValues);
                c.updateFields = GetStringArrayFromComObject(updateFields);
                c.updateValues = GetObjectArrayFromComObject(updateValues);
                long count = MongoDBWrapper.GetInstance().Update(c);
                return count;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        [ComVisible(true)]
        public long InsertObject(string server, string database, string collection, string json)
        {
            try
            {
                MongoDBQueryInfo c = new MongoDBQueryInfo();
                c.server = server;
                c.collection = collection;
                c.database = database;
                c.rawJson = json;
                long count = MongoDBWrapper.GetInstance().Insert(c);
                return count;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
    }
}