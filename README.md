# ExcelMongoDBUtil #



### Excel MongoDB reader/writer with COM ###

* Read or write from/to MongoDB in Excel VBA

### Excel RTD Server with data feed from MongoDB ###

* Read predefined MongoDB with conditions periodically, and distribute it to ExcelRTD client.